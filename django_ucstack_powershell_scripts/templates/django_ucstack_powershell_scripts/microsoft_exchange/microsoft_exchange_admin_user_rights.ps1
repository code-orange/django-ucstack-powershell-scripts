# Microsoft Exchange Admin User Rights - Tenant: {{ org_tag }}
$mailboxes = Get-Mailbox -OrganizationalUnit "OU={{ org_tag }},{{ hosting_base_dn }}"
$mailboxes | Add-MailboxPermission -User "{{ super_admin_dn }}" -AccessRights FullAccess -InheritanceType All
$mailboxes | Add-MailboxPermission -User "Admins@{{ org_tag }}" -AccessRights FullAccess -InheritanceType All
