from django.template import loader


def render_microsoft_exchange_admin_user_rights(
    org_tag: str, hosting_base_dn: str, super_admin_dn: str
):
    template = loader.get_template(
        "django_ucstack_powershell_scripts/microsoft_exchange/microsoft_exchange_admin_user_rights.ps1"
    )

    template_opts = dict()
    template_opts["org_tag"] = org_tag
    template_opts["hosting_base_dn"] = hosting_base_dn
    template_opts["super_admin_dn"] = super_admin_dn

    return template.render(template_opts)


def render_microsoft_exchange_full_access_everybody(org_tag: str, hosting_base_dn: str):
    template = loader.get_template(
        "django_ucstack_powershell_scripts/microsoft_exchange/microsoft_exchange_full_access_everybody.ps1"
    )

    template_opts = dict()
    template_opts["org_tag"] = org_tag
    template_opts["hosting_base_dn"] = hosting_base_dn

    return template.render(template_opts)
