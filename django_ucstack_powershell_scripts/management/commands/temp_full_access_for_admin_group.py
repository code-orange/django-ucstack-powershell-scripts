from django.core.management.base import BaseCommand

from django_directory_backend_client.django_directory_backend_client.func import (
    directory_backend_get_tenant,
)
from django_ucstack_models.django_ucstack_models.models import UcTenantSettings


class Command(BaseCommand):
    help = "Run: temp_full_access_for_admin_group"

    def handle(self, *args, **options):
        all_tenants = UcTenantSettings.objects.filter(customer__org_tag__isnull=False)

        for tenant in all_tenants:
            directory_tenant = directory_backend_get_tenant(
                tenant.customer.id, tenant.customer.org_tag
            )

            print(
                'get-mailbox -OrganizationalUnit "OU='
                + tenant.customer.org_tag
                + ',OU=Hosting,DC=intra,DC=srvfarm,DC=net" | '
                + 'Add-MailboxPermission -User "Admins@'
                + tenant.customer.org_tag
                + '" -AccessRights FullAccess -InheritanceType All'
            )

            print(
                'get-mailbox -OrganizationalUnit "OU='
                + tenant.customer.org_tag
                + ',OU=Hosting,DC=intra,DC=srvfarm,DC=net" | '
                + 'Add-MailboxPermission -User "'
                + directory_tenant["super_admin_upn"]
                + '" -AccessRights FullAccess -InheritanceType All'
            )
