# Microsoft Exchange Full Access Everybody - Tenant: {{ org_tag }}
$mailboxes = Get-Mailbox -OrganizationalUnit "OU={{ org_tag }},{{ hosting_base_dn }}"

foreach($mailbox in $mailboxes) {
    $dn = $mailbox.DistinguishedName
    $mailboxes | Add-MailboxPermission -User "$dn" -AccessRights FullAccess -InheritanceType All
    $mailboxes | Add-ADPermission -User "$dn" -AccessRights ExtendedRight -ExtendedRights "Send As"
    $mailboxes | Set-Mailbox -GrantSendOnBehalfTo @{Add="$dn"}
}
